FROM node:14.19.0-alpine as build
LABEL MAINTAINER="Mohammad Mahdi Mohammadi Fath|mohammad74mf@gmail.com|mohammad74mf@yahoo.com"

# install packages
ENV PYTHONUNBUFFERED=1
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json /app/package.json
RUN npm install --silent
COPY . /app
RUN npm run build

# build app for production with minification and deploy with nginx webserver
FROM nginx:1.21.0-alpine
COPY --from=build /app/dist /usr/share/nginx/html
COPY nginx.prod.conf /temp/prod.conf
RUN envsubst /app < /temp/prod.conf > /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
