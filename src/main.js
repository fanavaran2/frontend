import Vue from 'vue'
import App from './App.vue'
// BootstrapVue add
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.use(BootstrapVue);
import router from './router'
import store from './store'

Vue.config.productionTip = false
Vue.use(require("vue-jalali-moment"))

new Vue({
  router,
  store: store,
  render: h => h(App),
}).$mount('#app')
