import Vue from "vue";
import VueRouter from "vue-router";
import store from './store'

Vue.use(VueRouter);

import Home from './components/HomePage'
import Login from './components/LoginPage'
import Register from './components/RegisterPage'
import Topic from './components/TopicPage'
import Auther from './components/AutherPage'
import Book from './components/BookPage'
import Loan from './components/LoanPage'
import LoanList from './components/LoanListPage'



const routes = [
    { path: '/', component: Home, meta: { requiresAuth: true }, },
    { path: '/login', component: Login, meta: { requiresAuth: false }, },
    { path: '/register', component: Register, meta: { requiresAuth: false }, },
    { path: '/topic', component: Topic, meta: { requiresAuth: true }, },
    { path: '/auther', component: Auther, meta: { requiresAuth: true }, },
    { path: '/book', component: Book, meta: { requiresAuth: true }, },
    { path: '/loan', component: Loan, meta: { requiresAuth: true }, },
    { path: '/loan-list', component: LoanList, meta: { requiresAuth: true }, },
]
const router = new VueRouter({
    routes,
    mode: "history"
});

router.beforeEach((to, from, next) => {
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
    store.commit('SET_CURRENT')
    const CurrentUser = store.getters.getCurrent  
    if (requiresAuth && !CurrentUser) {
        next('/login');
    } else if (to.path == '/login' && CurrentUser) {
        next('/');
    } else {
        next();
    }
});

export default router;