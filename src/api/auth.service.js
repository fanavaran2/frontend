import axios from 'axios';
// import store from '../store'

const token = JSON.parse(localStorage.getItem('accessToken'))

const axiosBase = axios.create({
    baseURL: process.env.VUE_APP_API_URL,
})
axiosBase.defaults.xsrfCookieName = 'csrftoken'
axiosBase.defaults.xsrfHeaderName = 'X-CSRFToken'
axiosBase.defaults.headers.common['Authorization'] = 'Bearer ' + token


export { axiosBase }

