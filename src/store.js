import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        currentUser: '',
    },
    getters: {
        getCurrent: state => {
            const accessToken = JSON.parse(localStorage.getItem('accessToken'))
            state.currentUser = accessToken
            return state.currentUser
        },
        getUserId: state =>{
            var base64Url = state.currentUser.split('.')[1];
            var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
            var jsonPayload = decodeURIComponent(window.atob(base64).split('').map(function (c) {
                return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
            }).join(''));

            return JSON.parse(jsonPayload);
        }
    },
    mutations: {
        'SET_CURRENT'(state) {
            const accessToken = JSON.parse(localStorage.getItem('accessToken'))
            state.currentUser = accessToken
        },

    },
    actions: {

    }
})
